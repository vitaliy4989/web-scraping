# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
from carmax.items import CarmaxItem
import json, datetime, csv, random, base64, re

class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['carmax.com']
    useragent_lists = useragent.user_agent_list

    headers = {
        'authority': 'www.carmax.com',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9',
    }
    car_total = 0
    def start_requests(self):

        print ("===== Start =====")

        url = "https://www.carmax.com/cars/api/get?uri=%2Fcars%2Fall&skip=0&take=24&radius=90&zipCode=90301&sort=20"
        req = Request(url=url, callback=self.getItem, headers=self.headers, dont_filter=True)
        yield req

    def getItem(self, response):
        print ("===== get items =====")
        jsonData = json.loads(response.body)
        carLists = jsonData["items"]
        totalCount = jsonData["totalCount"]
        # print (totalCount)
        # print (len(carLists))

        for agent in carLists:
            stockNumber = agent["stockNumber"]
            link = "https://www.carmax.com/car/{}".format(stockNumber)
            req = Request(url=link, callback=self.parseItem, headers=self.headers, dont_filter=True)
            req.meta["stockNumber"] = stockNumber
            yield req

        cnt = int(totalCount)/24
        cntmod = int(totalCount)%24
        if cntmod > 0:
            cnt = cnt + 1

        for pageCnt in range(1, cnt + 1):
            count = pageCnt * 24
            url = "https://www.carmax.com/cars/api/get?uri=%2Fcars%2Fall&skip={}&take=24&radius=90&zipCode=90301&sort=20".format(count)
            req = Request(url=url, callback=self.getMoreItem, headers=self.headers, dont_filter=True)
            yield req

    def getMoreItem(self, response):
        print ("===== More Item =====")
        jsonData = json.loads(response.body)
        carLists = jsonData["items"]

        for agent in carLists:
            stockNumber = agent["stockNumber"]
            link = "https://www.carmax.com/car/{}".format(stockNumber)
            req = Request(url=link, callback=self.parseItem, headers=self.headers, dont_filter=True)
            req.meta["stockNumber"] = stockNumber
            yield req

        totalCount = jsonData["totalCount"]
        print ("totalCount === >>> ", totalCount)
        
    def parseItem(self, response):
        item = CarmaxItem()

        title = ''.join(response.xpath('//div[@class="car-page-header__car-title"]//text()').extract()).strip()
        price = response.xpath('//div[@class="price-mileage--price-container"]/div[@class="price-mileage--value"]/span/text()').extract_first()
        mileage = response.xpath('//div[@class="price-mileage--mileage-container"]/div[@class="price-mileage--value"]/text()').extract_first()
        typography = ''.join(response.xpath('//div[@class="store-information--title kmx-typography--display-1"]//text()').extract()).strip()
        location = ''.join(response.xpath('//div[@class="store-information--location"]//text()').extract()).strip()
        phone = response.xpath('//div[@class="store-information--phone"]/text()').extract_first()

        item["title"] = title
        item["price"] = price
        item["mileage"] = mileage
        item["typography"] = typography
        item["location"] = location
        item["phone"] = phone
        item["stockNumber"] = response.meta["stockNumber"]

        yield item