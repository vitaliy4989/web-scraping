# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CarmaxItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    price = scrapy.Field()
    mileage = scrapy.Field()
    typography = scrapy.Field()
    location = scrapy.Field()
    phone = scrapy.Field()
    stockNumber = scrapy.Field()