# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem

class CarmaxPipeline(object):
    count = 1
    def __init__(self):
        self.church_seen = set()

    def process_item(self, item, spider):
        carId = (item['stockNumber'] if item['stockNumber'] else '')
        if carId in self.church_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.church_seen.add(carId)
            return item