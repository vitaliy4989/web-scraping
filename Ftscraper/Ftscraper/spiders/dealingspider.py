# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
from Ftscraper.items import DealingsItem
import time, datetime, csv, random, base64, re, json
import sys
import MySQLdb
from scrapy.utils.project import get_project_settings

settings = get_project_settings()

dbargs = settings.get('DB_CONNECT')    
db_server = settings.get('DB_SERVER')    
db = MySQLdb.connect(dbargs['host'], dbargs['user'], dbargs['passwd'], dbargs['db'])
cursor = db.cursor()

class DealingspiderSpider(scrapy.Spider):
    name = "dealingspider"
    allowed_domains = ["ft.com"]

    useragent_lists = useragent.user_agent_list
    select_param = ""

    def __init__(self,  param = '', *args, **kwargs):

        super(DealingspiderSpider, self).__init__(*args, **kwargs)
        
        self.select_param = param

    def set_proxies(self, url, callback, headers=None):

        req = Request(url=url, callback=callback, dont_filter=True, headers= headers)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://ofurgody.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):

        if (self.select_param.isdigit()!=True and self.select_param!="all"):
            print("===== Please Insert Correct Command!!! =====")
            print("* Case Get All Data : scrapy crawl dealingspider -a param=all")
            print("* Case Get n Columns : scrapy crawl dealingspider -a param=n (ex: If you want to get 6 pages, param=6)")
            return

        with open('urlLists.csv') as csvfile:
            reader = csv.DictReader(csvfile)

            for listCount, row in enumerate(reader):

                Link = ''.join(row['Link']).strip()
                dbData = cursor.execute('SELECT secid FROM equities_data where Link="' + Link + '"')
                dbData = cursor.fetchall()
                if dbData:

                    secid =re.search(r'\d+', str(dbData)).group()

                    Symb = ''.join(row['Symb']).strip()
                    url = "https://markets.ft.com/data/equities/tearsheet/directors?s=" + Symb
                    req = self.set_proxies(url, self.getParam)

                    req.meta['secid'] = secid
                    # req.meta['url'] = url

                    yield req
                # if listCount>3:
                #     return 

    def getParam(self, response):
        self.logger.info("====== Get param =======")
        # c_url = response.meta['url']
        # print c_url
        secid = response.meta['secid']

        symbols = ''.join(response.xpath('//input[@name="symbols"]/@value').extract()).strip()
        url = "https://markets.ft.com/research/webservices/companies/v1/directordealings?officer=&position=&symbols=" + symbols + "&source=51d7791a57&limit=5&offset=0"
        req = self.set_proxies(url, self.getFirstData)

        req.meta['secid'] = secid
        req.meta['symbols'] = symbols

        yield req


    def getFirstData(self, response):
        self.logger.info("====== getFirstData =======")
        secid = response.meta['secid']
        symbols = response.meta['symbols']

        item = DealingsItem()

        json_data = json.loads(response.body)

        totalNumber = json_data["data"]["items"][0]["directorTransactions"]["totalItems"]
        # print "------------------"
        # print totalNumber

        if self.select_param == "all":
            # print "--------- get all ------------"

            item['secid'] = secid
            for itemdata in json_data["data"]["items"][0]["directorTransactions"]["items"]:
                try:
                    item['Date'] = itemdata["date"]
                except:
                    item['Date'] = ""

                try:
                    item['Type'] = itemdata["transType"]
                except:
                    item['Type'] = ""

                try:
                    item['Name'] = itemdata["officer"]
                except:
                    item['Nmae'] = ""

                try:
                    item['Title'] = itemdata["position"]
                except:
                    item['Title'] = ""

                try:
                    item['Shares'] = itemdata["numShares"]
                except:
                    item['Shares'] = ""

                try:
                    item['Pershare'] = itemdata["price"]
                except:
                    item['Pershare'] = ""

                try:
                    item['Dealsize'] = itemdata["totalValue"]
                except:
                    item['Dealsize'] = ""
                    
                yield item

            if totalNumber>5:
                # print "--------- get all totalNumber>5 ------------"

                num = int(totalNumber)/5
                # print num 9
                for count in range(1, num+1):

                    offset = str(count*5)
                    url = "https://markets.ft.com/research/webservices/companies/v1/directordealings?officer=&position=&symbols=" + symbols + "&source=51d7791a57&limit=5&offset=" + offset
                    req = self.set_proxies(url, self.getData)
                    req.meta['secid'] = secid
                    yield req
        else:
            if self.select_param=="1":
                # print "-------" + str(self.select_param) + "---------"
                item['secid'] = secid
                for itemdata in json_data["data"]["items"][0]["directorTransactions"]["items"]:
                    try:
                        item['Date'] = itemdata["date"]
                    except:
                        item['Date'] = ""

                    try:
                        item['Type'] = itemdata["transType"]
                    except:
                        item['Type'] = ""

                    try:
                        item['Name'] = itemdata["officer"]
                    except:
                        item['Nmae'] = ""

                    try:
                        item['Title'] = itemdata["position"]
                    except:
                        item['Title'] = ""

                    try:
                        item['Shares'] = itemdata["numShares"]
                    except:
                        item['Shares'] = ""

                    try:
                        item['Pershare'] = itemdata["price"]
                    except:
                        item['Pershare'] = ""

                    try:
                        item['Dealsize'] = itemdata["totalValue"]
                    except:
                        item['Dealsize'] = ""
                        
                    yield item

            else:
                # print "!!!-------" + str(self.select_param) + "---------!!!"

                item['secid'] = secid
                for itemdata in json_data["data"]["items"][0]["directorTransactions"]["items"]:
                    try:
                        item['Date'] = itemdata["date"]
                    except:
                        item['Date'] = ""

                    try:
                        item['Type'] = itemdata["transType"]
                    except:
                        item['Type'] = ""

                    try:
                        item['Name'] = itemdata["officer"]
                    except:
                        item['Nmae'] = ""

                    try:
                        item['Title'] = itemdata["position"]
                    except:
                        item['Title'] = ""

                    try:
                        item['Shares'] = itemdata["numShares"]
                    except:
                        item['Shares'] = ""

                    try:
                        item['Pershare'] = itemdata["price"]
                    except:
                        item['Pershare'] = ""

                    try:
                        item['Dealsize'] = itemdata["totalValue"]
                    except:
                        item['Dealsize'] = ""
                        
                    yield item

                if totalNumber>5:
                    # print "----------- totalNumber>5 -----------"
                    self.select_param = int(self.select_param) - 1
                    num = int(totalNumber)/5
                    # print num
                    if num>int(self.select_param):
                        # print "--------- num > self.select_param ----------"
                        num = int(self.select_param)
                        # print num
                        for count in range(1, num+1):

                            offset = str(count*5)
                            url = "https://markets.ft.com/research/webservices/companies/v1/directordealings?officer=&position=&symbols=" + symbols + "&source=51d7791a57&limit=5&offset=" + offset
                            req = self.set_proxies(url, self.getData)
                            req.meta['secid'] = secid
                            yield req 
                    else:
                        # print "--------- num < self.select_param ----------"

                        for count in range(1, num+1):

                            offset = str(count*5)
                            url = "https://markets.ft.com/research/webservices/companies/v1/directordealings?officer=&position=&symbols=" + symbols + "&source=51d7791a57&limit=5&offset=" + offset
                            req = self.set_proxies(url, self.getData)
                            req.meta['secid'] = secid
                            yield req 

    def getData(self, response):
        self.logger.info("====== getData =======")

        secid = response.meta['secid']

        item = DealingsItem()

        json_data = json.loads(response.body)
        item['secid'] = secid

        for itemdata in json_data["data"]["items"][0]["directorTransactions"]["items"]:
            try:
                item['Date'] = itemdata["date"]
            except:
                item['Date'] = ""

            try:
                item['Type'] = itemdata["transType"]
            except:
                item['Type'] = ""

            try:
                item['Name'] = itemdata["officer"]
            except:
                item['Nmae'] = ""

            try:
                item['Title'] = itemdata["position"]
            except:
                item['Title'] = ""

            try:
                item['Shares'] = itemdata["numShares"]
            except:
                item['Shares'] = ""

            try:
                item['Pershare'] = itemdata["price"]
            except:
                item['Pershare'] = ""

            try:
                item['Dealsize'] = itemdata["totalValue"]
            except:
                item['Dealsize'] = ""

            yield item        