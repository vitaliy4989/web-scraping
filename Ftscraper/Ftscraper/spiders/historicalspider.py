# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
from Ftscraper.items import HistoricalItem
from scrapy.selector import Selector
import time, datetime, csv, random, base64, re, json
from datetime import timedelta
import sys
import MySQLdb
from scrapy.utils.project import get_project_settings

settings = get_project_settings()

dbargs = settings.get('DB_CONNECT')    
db_server = settings.get('DB_SERVER')    
db = MySQLdb.connect(dbargs['host'], dbargs['user'], dbargs['passwd'], dbargs['db'])
cursor = db.cursor()

class HistoricalspiderSpider(scrapy.Spider):
    name = "historicalspider"
    allowed_domains = ["ft.com"]
    useragent_lists = useragent.user_agent_list
    select_param = ""

    def __init__(self,  param = '', *args, **kwargs):

        super(HistoricalspiderSpider, self).__init__(*args, **kwargs)
        
        self.select_param = param

    def set_proxies(self, url, callback, headers=None):

        req = Request(url=url, callback=callback, dont_filter=True, headers= headers)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://ofurgody.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):
        self.clearLog()
        self.makeLog("=================== Start ===================")

        with open('urlLists.csv') as csvfile:
            reader = csv.DictReader(csvfile)

            for listCount, row in enumerate(reader):

                Link = ''.join(row['Link']).strip()
                dbData = cursor.execute('SELECT secid FROM equities_data where Link="' + Link + '"')
                dbData = cursor.fetchall()
                if dbData:

                    secid =re.search(r'\d+', str(dbData)).group()

                    Symb = ''.join(row['Symb']).strip()
                    url = "https://markets.ft.com/data/equities/tearsheet/historical?s=" + Symb
                    req = self.set_proxies(url, self.getHistoricalData)
                    # print url
                    req.meta['secid'] = secid
                    req.meta['Symb'] = Symb

                    yield req
                if listCount==1:
                    return 
    def getHistoricalData(self, response):
        self.logger.info("====== getHistoricalData ======")
        secid = response.meta['secid']
        Symb = response.meta['Symb']

        getParams = ''.join(response.xpath('//div[@data-module-name="HistoricalPricesApp"]/@data-mod-config').extract()).strip()
        if getParams:
            
            getParam1 = re.search(r'"inception":"(.*?)"', getParams, re.M|re.I|re.S)
            inception = getParam1.group(1).split("T")[0]
            getParam2 = re.search(r'"symbol":"(.*?)"', getParams, re.M|re.I|re.S)
            symbol = getParam2.group(1)        

            startDate = datetime.datetime.strptime(inception, "%Y-%m-%d")
            # print "----------- startDate > " + str(startDate)

            currDate = datetime.datetime.now()
            # print "----------- currDate > " + str(currDate)
            if self.select_param=='' or self.select_param=='1':
                # print "------ none -------"
                preDate = currDate.replace(currDate.year - 1)
                if preDate < startDate:
                    preDate = startDate
                # print "----------- preDate > " + str(preDate) 
                sDate = "%s %s %s" % (preDate.year, preDate.month, preDate.day)
                # print "----------- sDate > " + str(sDate)
                eDate = "%s %s %s" % (currDate.year, currDate.month, currDate.day)
                # print "----------- eDate > " + str(eDate)

                url = "https://markets.ft.com/data/equities/ajax/get-historical-prices?startDate=" + sDate + "&endDate=" + eDate + "&symbol=" + str(symbol)
                req = self.set_proxies(url, self.getData)
                req.meta['secid'] = secid

                yield req

                logtxt = Symb + ", " + sDate + " ~ " + eDate
                self.makeLog(logtxt)

            elif "years" in str(self.select_param):
                # print "-------------------"
                # if (self.select_param.isdigit()!=True):
                #     print("===== Please Insert Correct Command!!! =====")
                #     print("* Case Get Data of 1 year : scrapy crawl historicalspider")
                #     print("* Case Get Data of n year : scrapy crawl historicalspider -a param=n (ex: If you want to get 6 years, param=6)")
                #     return  

                diff = int(filter(str.isdigit, str(self.select_param)))
                initDate = currDate.replace(currDate.year - diff)   
                if initDate < startDate:
                    initDate = startDate
                # print "----------- initDate > " + str(initDate)
                betweenYear = currDate.year - initDate.year
                # print "----------- betweenYear > " + str(betweenYear)

                sDate = "%s %s %s" % (initDate.year, initDate.month, initDate.day)
                sLogDate = "%s %s %s" % (initDate.year, initDate.month, initDate.day)
                # print "----------- sDate > " + str(sDate)

                nextDate = initDate.replace(initDate.year + 1)
                # print "----------- nextDate > " + str(nextDate) 
                for count in range(0, betweenYear):
                    # print "-------------------------"
                    # print "----------- sDate > " + str(sDate)

                    eDate = "%s %s %s" % (nextDate.year, nextDate.month, nextDate.day)
                    # print "----------- eDate > " + str(eDate)
                                
                    url = "https://markets.ft.com/data/equities/ajax/get-historical-prices?startDate=" + sDate + "&endDate=" + eDate + "&symbol=" + str(symbol)
                    req = self.set_proxies(url, self.getData)
                    req.meta['secid'] = secid

                    yield req

                    sDate = eDate

                    nextDate = nextDate.replace(nextDate.year + 1)
                    # print "===== nextDate > " + str(nextDate) 

                    eDate = "%s %s %s" % (nextDate.year, nextDate.month, nextDate.day)
                    # print "===== eDate > " + str(eDate)   
                
                eLogDate = nextDate.replace(nextDate.year - 1)
                eLogDate = "%s %s %s" % (eLogDate.year, eLogDate.month, eLogDate.day)
                # print eLogDate

                logtxt = Symb + ", " + sLogDate + " ~ " + eLogDate
                self.makeLog(logtxt)
                
            elif "days" in str(self.select_param):

                diff = int(filter(str.isdigit, str(self.select_param)))
                initDate = currDate - timedelta(days=diff)
                sDate = "%s %s %s" % (initDate.year, initDate.month, initDate.day)
                eDate = "%s %s %s" % (currDate.year, currDate.month, currDate.day)

                url = "https://markets.ft.com/data/equities/ajax/get-historical-prices?startDate=" + sDate + "&endDate=" + eDate + "&symbol=" + str(symbol)
                req = self.set_proxies(url, self.getData)
                req.meta['secid'] = secid

                yield req

                logtxt = Symb + ", " + sDate + " ~ " + eDate
                self.makeLog(logtxt)
            else:
                print "===== Insert correct param!!!"
                return

    def getData(self, response):
        self.logger.info("====== getData ======")
        item = HistoricalItem()
        secid = response.meta['secid']

        json_data = json.loads(response.body)
        htmlText = Selector(text=json_data["html"])

        item['secid'] = secid
        itemPaths = htmlText.xpath('//tr')

        for itemPath in itemPaths:
            Date = ''.join(itemPath.xpath('td[1]/span[1]/text()').extract()).strip()
            item['Date'] = Date
            
            Open = ''.join(itemPath.xpath('td[2]/text()').extract()).strip()
            item['Open'] = Open
            
            High = ''.join(itemPath.xpath('td[3]/text()').extract()).strip()
            item['High'] = High
            
            Low = ''.join(itemPath.xpath('td[4]/text()').extract()).strip()
            item['Low'] = Low
            
            Close = ''.join(itemPath.xpath('td[5]/text()').extract()).strip()
            item['Close'] = Close
            
            Volume = ''.join(itemPath.xpath('td[6]/span[1]/text()').extract()).strip()
            item['Volume'] = Volume
            
            yield item

    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()