# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class StockchartsItem(scrapy.Item):
    # define the fields for your item here like:
    Date = scrapy.Field()
    Symbol = scrapy.Field()
    Name = scrapy.Field()
    Action = scrapy.Field()
    Description = scrapy.Field()