# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
from Stockcharts.items import StockchartsItem
import time, datetime, csv, random, base64, json, re
import MySQLdb
from scrapy.utils.project import get_project_settings

settings = get_project_settings()

dbargs = settings.get('DB_CONNECT')    
db_server = settings.get('DB_SERVER')    
db = MySQLdb.connect(dbargs['host'], dbargs['user'], dbargs['passwd'], dbargs['db'])
cursor = db.cursor()

class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['stockcharts.com']
    useragent_lists = useragent.user_agent_list

    def set_proxies(self, url, callback, headers=None):

        req = Request(url=url, callback=callback, headers=headers, dont_filter=True)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://onkycend.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent
        return req

    def start_requests(self):

        self.clearLog()
        self.makeLog("=================== Start ===================")
        dbData = cursor.execute('TRUNCATE freecharts_data')
        url = 'http://stockcharts.com/freecharts/adjusthist.php?search=*&description=&day=-7'
        req = self.set_proxies(url, self.getData)
        yield req

    def getData(self, response):
        item = StockchartsItem()
        pathList = response.xpath('//table[@class="sortableTable"]/tr')
        for i, ele in enumerate(pathList):
            if i == 0:
                continue
            item['Date'] = ele.xpath('td[2]/text()').extract_first()
            item['Symbol'] = ele.xpath('td[3]/text()').extract_first()
            item['Name'] = ele.xpath('td[4]/text()').extract_first()
            item['Action'] = ele.xpath('td[5]/text()').extract_first()
            item['Description'] = ele.xpath('td[6]/text()').extract_first()

            yield item

        self.makeLog('=== End ===')
    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()  
