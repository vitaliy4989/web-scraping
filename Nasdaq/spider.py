# -*- coding: utf-8 -*-
import MySQLdb
import requests
import time, csv, datetime, random, base64, re, json
import lxml.etree
import lxml.html
import argparse
import os
import os.path
import sys

reload(sys)  
sys.setdefaultencoding('utf8')

# --- test (local db)---
db_host = "localhost"
db_user = "root"
# db_pwd = "root"
db_pwd = ""
db_database = "nasdaq_db"
db_table = "historical_data"

db = MySQLdb.connect(host=db_host, user=db_user, passwd=db_pwd, db=db_database)
cursor = db.cursor()

# proxy_auth = "quantzero:gnusmas"
proxy_auth = 'pp-eiffykey:reyerobf'
def main(column_number):

    dbData = cursor.execute('TRUNCATE historical_data')

    # proxy = "165.231.103.111:80"        
    proxy = "onkycend.proxysolutions.net:34572"        
    proxies = {'http':'http://{}@{}'.format(proxy_auth, proxy), 'https':'http://{}@{}'.format(proxy_auth, proxy)}

    myfile = open("url_list.csv", "rb")
    urllist = csv.reader(myfile)

    for i, url in enumerate(urllist):
        print ("=======================")
        real_link = url[0]
        print ("real_link ==>>", real_link)
        try:
            response = requests.get(real_link, proxies=proxies)
        except:
            pass
        if response.status_code == 200:
            root = lxml.html.fromstring(response.text)

            dataList = root.xpath('//div[@id="historicalContainer"]//table/tbody/tr')
            count = 0
            sid = i + 1
            if column_number == None:

                for element in dataList:
                    print ("-------------------")
                    try:
                        Date = element.xpath('td[1]/text()')[0].strip()
                    except:
                        Date = ''

                    if ':' in Date:
                        continue        

                    try:
                        Open = element.xpath('td[2]/text()')[0].strip()
                    except:
                        Open = ''                
                    try:
                        High = element.xpath('td[3]/text()')[0].strip()
                    except:
                        High = ''
                    try:
                        Low = element.xpath('td[4]/text()')[0].strip()
                    except:
                        Low = ''
                    try:
                        Close_Last = element.xpath('td[5]/text()')[0].strip()
                    except:
                        Close_Last = ''

                    try:
                        Volume = element.xpath('td[6]/text()')[0].strip()
                    except:
                        Volume = ''


                    sql = "INSERT INTO " + db_table + " (sid, Date, Open, High, Low, Close_Last, Volume) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                    cursor.execute(sql, (sid, Date, Open, High, Low, Close_Last, Volume))
                    print (sid, Date, Open, High, Low, Close_Last, Volume)
                    db.commit()

                    count = count + 1
                    if count == 5:
                        break
            else:

                for element in dataList:
                    print ("-------------------")
                    try:
                        Date = element.xpath('td[1]/text()')[0].strip()
                    except:
                        Date = ''

                    if ':' in Date:
                        continue        

                    try:
                        Open = element.xpath('td[2]/text()')[0].strip()
                    except:
                        Open = ''                
                    try:
                        High = element.xpath('td[3]/text()')[0].strip()
                    except:
                        High = ''
                    try:
                        Low = element.xpath('td[4]/text()')[0].strip()
                    except:
                        Low = ''
                    try:
                        Close_Last = element.xpath('td[5]/text()')[0].strip()
                    except:
                        Close_Last = ''

                    try:
                        Volume = element.xpath('td[6]/text()')[0].strip()
                    except:
                        Volume = ''


                    sql = "INSERT INTO " + db_table + " (sid, Date, Open, High, Low, Close_Last, Volume) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                    cursor.execute(sql, (sid, Date, Open, High, Low, Close_Last, Volume))
                    print (sid, Date, Open, High, Low, Close_Last, Volume)
                    db.commit()

                    count = count + 1
                    if count == int(column_number):
                        break

            makeLog(str(sid));
            # with open("response.html", 'w') as f:
            #     f.write(response.text.encode("utf-8"))
    db.close()
    print " ===== END ===== "


       
def makeLog(txt):

    standartdate = datetime.datetime.now()
    date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
    fout = open("log.txt", "a")
    fout.write(str(date) + " -> " + txt + "\n")
    fout.close()

def clearLog():
    fout = open("log.txt", "w")
    fout.close()   

if __name__ == '__main__':

    clearLog()
    makeLog("=================== Start ===================")
    parser = argparse.ArgumentParser(description="Do something.")
    parser.add_argument('-p', '--param', type=int, required=False, default=None, help='Number of Column, defaults to 1')

    args = parser.parse_args()
    # Set config
    column_number = args.param

    main(column_number)