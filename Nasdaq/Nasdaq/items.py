# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class NasdaqItem(scrapy.Item):
    # define the fields for your item here like:
    Date = scrapy.Field()
    Open = scrapy.Field()
    High = scrapy.Field()
    Low = scrapy.Field()
    Close_Last = scrapy.Field()
    Volume = scrapy.Field()
    sid   = scrapy.Field()