# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
from Nasdaq.items import NasdaqItem
import time, datetime, csv, random, base64
import MySQLdb
from scrapy.utils.project import get_project_settings

settings = get_project_settings()

dbargs = settings.get('DB_CONNECT')    
db_server = settings.get('DB_SERVER')    
db = MySQLdb.connect(dbargs['host'], dbargs['user'], dbargs['passwd'], dbargs['db'])
cursor = db.cursor()

class InvestingspiderSpider(scrapy.Spider):
    name = "spider"
    allowed_domains = ['nasdaq.com']

    select_param = ""
    useragent_lists = useragent.user_agent_list

    def __init__(self,  param = None, *args, **kwargs):

        super(InvestingspiderSpider, self).__init__(*args, **kwargs)
        
        self.select_param = param

    def set_proxies(self, url, callback, headers=None):

        req = Request(url=url, callback=callback, headers=headers, dont_filter=True)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://onkycend.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):

        self.clearLog()
        self.makeLog("=================== Start ===================")

        dbData = cursor.execute('TRUNCATE historical_data')

        myfile = open("url_list.csv", "rb")
        urllist = csv.reader(myfile)

        for i, url in enumerate(urllist):

            url=''.join(url).strip()
            # req = Request(url, callback=self.getData, dont_filter=True)
            req = self.set_proxies(url, self.getData)
            req.meta['number'] = i+1

            yield req    

    def getData(self, response):

        number = response.meta['number']

        item = NasdaqItem()
        table = response.xpath('//div[@id="historicalContainer"]//table/tbody/tr')
        item['sid'] = number

        if self.select_param == None:
            for cnt, ele in enumerate(table):
                item['Date'] = ele.xpath('td[1]/text()').extract_first().strip()
                if ':' in item['Date']:
                    continue
                item['Open'] = ele.xpath('td[2]/text()').extract_first().strip()
                item['High'] = ele.xpath('td[3]/text()').extract_first().strip()
                item['Low'] = ele.xpath('td[4]/text()').extract_first().strip()
                item['Close_Last'] = ele.xpath('td[5]/text()').extract_first().strip()
                item['Volume'] = ele.xpath('td[6]/text()').extract_first().strip()

                yield item

                if cnt == 5:
                    break
        else:
            for cnt, ele in enumerate(table):


                item['Date'] = ele.xpath('td[1]/text()').extract_first().strip()
                if ':' in item['Date']:
                    continue

                item['Open'] = ele.xpath('td[2]/text()').extract_first().strip()
                item['High'] = ele.xpath('td[3]/text()').extract_first().strip()
                item['Low'] = ele.xpath('td[4]/text()').extract_first().strip()
                item['Close_Last'] = ele.xpath('td[5]/text()').extract_first().strip()
                item['Volume'] = ele.xpath('td[6]/text()').extract_first().strip()

                yield item

                if cnt == int(self.select_param):
                    break                
        self.makeLog(str(number))
       
    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()   