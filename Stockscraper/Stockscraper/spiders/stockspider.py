# -*- coding: utf-8 -*-
import scrapy
import useragent
from Stockscraper.items import StockscraperItem
from scrapy.http import Request, FormRequest
from copy import deepcopy
from time import sleep
import time, re, random, base64, datetime
import csv, json

class StockspiderSpider(scrapy.Spider):
    name = "stockspider"
    allowed_domains = ["bloomberg.com"]
    start_urls = (
        'http://www.bloomberg.com/',
    )
    useragent_lists = useragent.user_agent_list

    def set_proxies(self, url, callback):

        req = Request(url=url, callback=callback, dont_filter=True)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://ofurgody.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):

        self.clearLog()
        self.makeLog("=================== Start ===================")

        # read varid from csv file
        # myfile = open("varLists.csv", "rb")
        # urlLists = csv.reader(myfile)

        # for variable in urlLists:

        #     variable = ''.join(variable).strip()
        #     url = "https://search.bloomberg.com/lookup.json?callback=jQuery111302226115020039916_1502430501741&sites=bbiz&query=" + variable + "&frag_size=192&source=nmil7ewdr0s&types=company_public%2CIndex%2CCommodity%2CBond%2CCurrency%2CFund&group_size=10%2C10%2C10%2C10%2C10%2C10&fields=name%2Cslug%2Cticker_symbol%2Curl%2Corganization%2Ctitle&highlight=1&_=1502430501745"
        #     req = self.set_proxies(url, self.getData)
        #     req.meta['variable'] = variable
        #     yield req  

        #     return

        url = "https://search.bloomberg.com/lookup.json?callback=jQuery111302226115020039916_1502430501741&sites=bbiz&query=muni&frag_size=192&source=iy4dye1acz&types=company_public%2CIndex%2CCommodity%2CBond%2CCurrency%2CFund&group_size=10%2C10%2C10%2C10%2C10%2C10&fields=name%2Cslug%2Cticker_symbol%2Curl%2Corganization%2Ctitle&highlight=1&_=1502641472625"
        req = self.set_proxies(url, self.getData)
        req.meta['variable'] = "muni"
        yield req  

    def getData(self, response):
        print "======= Get data ======="
        item = StockscraperItem()

        variable = response.meta['variable']
        item['Variable'] = variable

        sub_data = re.search("jQuery111302226115020039916_1502430501741\((.*\])\)", response.body, re.M|re.S).group(1)
        jsonData = json.loads(sub_data)

        for elements in jsonData:
            # print "------------"
            # print element
            for element in elements["results"]:
                print "-----------------------"
                name = element["name"]
                name = name.replace("<em>","").replace("</em>","")
                item['Name'] = name
                # print name

                # score = element["score"]
                # item['Score'] = score
                # print score

                ticker_symbol = element["ticker_symbol"]
                ticker_symbol = ticker_symbol.replace("<em>","").replace("</em>","")
                item['Ticker'] = ticker_symbol
                # print ticker_symbol

                url = element["url"]
                item['Url'] = url
                # print url

                yield item

            # yield item

        # for element in jsonData[0]["results"]:
        #     # print "-----------------------"
        #     name = element["name"]
        #     name = name.replace("<em>","").replace("</em>","")
        #     item['Name'] = name
        #     # print name

        #     # score = element["score"]
        #     # item['Score'] = score
        #     # print score

        #     ticker_symbol = element["ticker_symbol"]
        #     ticker_symbol = ticker_symbol.replace("<em>","").replace("</em>","")
        #     item['Ticker'] = ticker_symbol
        #     # print ticker_symbol

        #     url = element["url"]
        #     item['Url'] = url
        #     # print url

        #     yield item

        # for element in jsonData[5]["results"]:
        #     # print "-----------------------"
        #     name = element["name"]
        #     name = name.replace("<em>","").replace("</em>","")
        #     item['Name'] = name
        #     # print name

        #     # score = element["score"]
        #     # item['Score'] = score
        #     # print score

        #     ticker_symbol = element["ticker_symbol"]
        #     ticker_symbol = ticker_symbol.replace("<em>","").replace("</em>","")
        #     item['Ticker'] = ticker_symbol
        #     # print ticker_symbol

        #     url = element["url"]
        #     item['Url'] = url
        #     # print url

        #     yield item

        logtxt = variable
        self.makeLog(logtxt)
    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()            
