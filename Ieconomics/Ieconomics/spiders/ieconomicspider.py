# -*- coding: utf-8 -*-
import scrapy
import useragent
from scrapy.http import Request, FormRequest
import time, datetime, csv, random, base64, re, json, requests
import sys
import MySQLdb
from scrapy.utils.project import get_project_settings
from scrapy.selector import Selector

settings = get_project_settings()

dbargs = settings.get('DB_CONNECT')    
db_server = settings.get('DB_SERVER')    
db = MySQLdb.connect(dbargs['host'], dbargs['user'], dbargs['passwd'], dbargs['db'])
cursor = db.cursor()
ipsCount = 0

class IeconomicspiderSpider(scrapy.Spider):
    name = "ieconomicspider"
    allowed_domains = ["ieconomics.com"]
    select_param = ""
    useragent_lists = useragent.user_agent_list
    def __init__(self,  param = '', *args, **kwargs):

        super(IeconomicspiderSpider, self).__init__(*args, **kwargs)
        
        self.select_param = param

    def set_proxies(self, url, callback, headers=None):

        req = Request(url=url, callback=callback, dont_filter=True, headers= headers)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://onkycend.proxysolutions.net:xxxxx"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):

        if (self.select_param.isdigit()!=True and self.select_param!="all"):
            print(" ================ Please Insert Correct Mode!!! ================ ")
            print("* Case Get All Data : scrapy crawl ieconomicspider -a param=all")
            print("* Case Get last n : scrapy crawl ieconomicspider -a param=n (ex: If you want to get last 6 columns, param=6)")
            return  

        global ipsCount
        # self.clearLog()
        # self.makeLog("=================== Start ===================")
        # dbData = cursor.execute('SELECT ips FROM first_table ORDER BY ips DESC LIMIT 1')
        # dbData = cursor.fetchall()
        # if dbData:

        #     ipsCount = int(re.search(r'\d+', str(dbData)).group()) + 1
        # else:
        #     ipsCount = 1 

        headers = {
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
        }

        with open('lists.csv') as csvfile:
            reader = csv.DictReader(csvfile)

            for listCount, row in enumerate(reader):

                country = ''.join(row['country']).strip()
                variable = ''.join(row['variable']).strip()

                url = "https://ieconomics.com/" + country + "-" + variable
                # req = self.set_proxies(url, self.getParam)
                response = requests.get(url, headers=headers, allow_redirects=False, verify=False)
                file = open('re.html','w') 
                file.write(response.text.encode('utf-8')) 
                # htmlBody = Selector(text=response.text)
                auth = re.search('\"AUTH\"\:\ "(.*?)"\}', response.content, re.I|re.M|re.S)
                print auth
                return
                req.meta['variableID'] = listCount + 1
                req.meta['url'] = url

                yield req
                if listCount>1: 
                    return

    def getParam(self, response):
        self.logger.info("============= getParam ===============")
        ts = time.strftime("%s", time.gmtime())
        auth = re.search('headers\:\ \{"AUTH"\:\ "(.*?)"\}', response.body, re.I/re.M|re.S).group(1)
        print auth
        headers = {
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'AUTH': auth,
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Connection': 'keep-alive',
        }

        variableID = response.meta['variableID']
        check_url = response.meta['url']
        params = response.xpath('//span[@class="grid click-baiter onecolumn"]/@data-iids').extract()
        # print params
        if len(params)==0:
            # print "-------- None --------"
            # print check_url
            params = response.xpath('//span[@class="grid click-baiter twocolumns"]/@data-iids').extract()
            # print params
            if len(params)>0:
                for count, param in enumerate(params):

                    url = "https://markets.tradingeconomics.com/econ/?s=" + param + "&span=max&lang=en&forecast=true&_={}".format(ts)
                    req = self.set_proxies(url, self.getData, headers=headers)

                    req.meta['chart_Num'] = count + 1
                    req.meta['variableID'] = variableID
                    req.meta['check_url'] = check_url
                    # req.meta['json_url'] = url

                    yield req
                self.makeLog(check_url)         

            # return
        else :

            for count, param in enumerate(params):

                url = "https://markets.tradingeconomics.com/econ/?s=" + param + "&span=max&lang=en&forecast=true&_={}".format(ts)
                req = self.set_proxies(url, self.getData, headers=headers)

                req.meta['chart_Num'] = count + 1
                req.meta['variableID'] = variableID
                req.meta['check_url'] = check_url
                # req.meta['json_url'] = url

                yield req
            self.makeLog(check_url)         

    def getData(self, response):
        self.logger.info("============= getData ===============")
        global ipsCount

        variableID = response.meta['variableID']
        chart_Num = response.meta['chart_Num']
        check_url = response.meta['check_url']
        # json_url = response.meta['json_url']
        # print check_url
        # print json_url

        json_data = json.loads(response.body)
        try:
            category = json_data[0]["series"][0]["serie"]["name"]
        except:
            # print "-------- Json Error ---------"
            # print check_url
            return
        unit = json_data[0]["series"][0]["serie"]["unit"]
        if unit:
            Chartname = category + " (" + unit.capitalize() + ")"
        else:
            Chartname = category

        sql = "INSERT INTO first_table (ips, VariableID, ChartID, Chartname) VALUES (%s, %s, %s, %s)"
        cursor.execute(sql, (ipsCount, variableID, chart_Num, Chartname))
        db.commit()
        
        if self.select_param=="all":

            for cc, every in enumerate(json_data[0]["series"][0]["serie"]["data"]):

                date = every["date"]
                close = every["close"]

                sql = "INSERT INTO iconomics_data (chartID, variable, date, value) VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (chart_Num, ipsCount, date, close))
                db.commit()

        else:
            dateList = []
            closeList = []
            for cc, every in enumerate(json_data[0]["series"][0]["serie"]["data"]):

                date = every["date"]
                dateList.append(date)

                close = every["close"]
                closeList.append(close)

            for cc, count in enumerate(range(int(len(dateList))-1,0,-1)):

                if cc==int(self.select_param):
                    break

                date = dateList[count]
                close = closeList[count]

                sql = "INSERT INTO iconomics_data (chartID, variable, date, value) VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (chart_Num, ipsCount, date, close))
                db.commit()
   
        ipsCount = ipsCount + 1
    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()


# Greece, manufacturing pmi
# spain, Wages In Manufacturing
# italy, Unemployment Rate
# Greece, inflation rate
# https://ieconomics.com/us-7-year-note-yield#