# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DataItem(scrapy.Item):
    # define the fields for your item here like:
    date = scrapy.Field()
    value = scrapy.Field()
    chartID = scrapy.Field()
    variable = scrapy.Field()
    
class ChartItem(scrapy.Item):
    # define the fields for your item here like:
    VariableID = scrapy.Field()
    ChartID = scrapy.Field()
    Chartname = scrapy.Field()
    ips = scrapy.Field()

class IeconomicsItem(scrapy.Item):
    # define the fields for your item here like:
    ips = scrapy.Field()
    ChartID = scrapy.Field()
    VariableID = scrapy.Field()
    Chartname = scrapy.Field()
    
    chartID = scrapy.Field()
    variable = scrapy.Field()
    date = scrapy.Field()
    value = scrapy.Field()