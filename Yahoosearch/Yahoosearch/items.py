# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class YahoosearchItem(scrapy.Item):
    # define the fields for your item here like:
    Variable = scrapy.Field()
    Exch = scrapy.Field()
    ExchDisp = scrapy.Field()
    Name = scrapy.Field()
    Symbol = scrapy.Field()
    Url = scrapy.Field()