# -*- coding: utf-8 -*-
import scrapy
import useragent
from Yahoosearch.items import YahoosearchItem
from scrapy.http import Request, FormRequest
from copy import deepcopy
from time import sleep
import time, re, random, base64, datetime
import csv, json

class YahoospiderSpider(scrapy.Spider):
    name = "yahoospider"
    allowed_domains = ["yahoo.com"]

    useragent_lists = useragent.user_agent_list

    def set_proxies(self, url, callback):

        req = Request(url=url, callback=callback, dont_filter=True)
        user_pass=base64.encodestring(b'pp-eiffykey:reyerobf').strip().decode('utf-8')
        req.meta['proxy'] = "http://ofurgody.proxysolutions.net:11355"
        req.headers['Proxy-Authorization'] = 'Basic ' + user_pass
        user_agent = self.useragent_lists[random.randrange(0, len(self.useragent_lists))]
        req.headers['User-Agent'] = user_agent

        return req

    def start_requests(self):

        self.clearLog()
        self.makeLog("=================== Start ===================")

        # read varid from csv file
        myfile = open("varLists.csv", "rb")
        urlLists = csv.reader(myfile)

        for variable in urlLists:

            variable = ''.join(variable).strip()
            url = "https://finance.yahoo.com/_finance_doubledown/api/resource/searchassist;searchTerm=" + variable + "?bkt=%5B%22fin-strm-ctrl%22%2C%22fndmtest%22%2C%22finnossl%22%2C%22fin-modal-001-copy%22%2C%22libra%22%5D&device=desktop&feature=canvassOffnet%2CenableChartiqPromo%2CnewContentAttribution%2CrelatedVideoFeature%2CvideoNativePlaylist%2Clivecoverage%2CenableSingleRail&intl=us&lang=en-US&partner=none&prid=fjqmv35cpdonf&region=US&site=finance&tz=Europe%2FZurich&ver=0.102.532&returnMeta=true"
            req = self.set_proxies(url, self.getData)
            req.meta['variable'] = variable
            yield req  

            # return

        # test
        # url = "https://finance.yahoo.com/_finance_doubledown/api/resource/searchassist;searchTerm=goog?bkt=%5B%22fin-strm-ctrl%22%2C%22fndmtest%22%2C%22finnossl%22%2C%22fin-modal-001-copy%22%2C%22libra%22%5D&device=desktop&feature=canvassOffnet%2CenableChartiqPromo%2CnewContentAttribution%2CrelatedVideoFeature%2CvideoNativePlaylist%2Clivecoverage%2CenableSingleRail&intl=us&lang=en-US&partner=none&prid=fjqmv35cpdonf&region=US&site=finance&tz=Europe%2FZurich&ver=0.102.532&returnMeta=true"
        # req = self.set_proxies(url, self.getData)
        # req.meta['variable'] = "goog"
        # yield req  

    def getData(self, response):
        print "======= Get data ======="
        item = YahoosearchItem()

        variable = response.meta['variable']
        item['Variable'] = variable

        jsonData = json.loads(response.body)

        for element in jsonData["data"]["items"]:
            # print "------------"
            # print element
            print "-----------------------"
            name = element["name"]
            item['Name'] = name
            # print name

            exch = element["exch"]
            item['Exch'] = exch
            # print exch

            exchDisp = element["exchDisp"]
            item['ExchDisp'] = exchDisp
            # print exchDisp

            symbol = element["symbol"]
            item['Symbol'] = symbol
            # print symbol

            url = "https://finance.yahoo.com/quote/" + symbol + "?p=" + symbol
            item['Url'] = url
            # print url

            yield item

        logtxt = variable
        self.makeLog(logtxt)

    def makeLog(self, txt):

        standartdate = datetime.datetime.now()
        date = standartdate.strftime('%Y-%m-%d %H:%M:%S')
        fout = open("log.txt", "a")
        fout.write(str(date) + " -> " + txt + "\n")
        fout.close()

    def clearLog(self):
        fout = open("log.txt", "w")
        fout.close()            
