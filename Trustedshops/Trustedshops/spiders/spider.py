# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request, FormRequest
from Trustedshops.items import TrustedshopsItem
import time, datetime, csv, random, base64, re

class SpiderSpider(scrapy.Spider):
    name = 'spider'
    start_urls = ['https://www.trustedshops.de/shops/']

    def parse(self, response):
        print " ===== Start ===== "
        cateList = response.xpath('//div[@class="link-list"]/ul[@class="row"]/li/a')
        # print len(cateList)
        for element in cateList:
            itemUrl = response.urljoin(element.xpath('@href').extract_first())

            req = Request(url=itemUrl, callback=self.getCateData, dont_filter=True)
            yield req

            # return

    def getCateData(self, response):
        # print "--- getCateData ---"
        # print response.url
        eleList = response.xpath('//div[@class="search-results"]/div[@class="row"]/div[@class="col-xs-12"]')
        # print len(eleList)
        for element in eleList:
            itemUrl = element.xpath('a/@href').extract_first()
            req = Request(url=itemUrl, callback=self.getDetailData, dont_filter=True)
            req.meta['check_url'] = itemUrl            

            yield req

            # return
        nextUrl = response.xpath('//i[@class="fa fa-angle-right"]/parent::a/@href').extract_first()
        if nextUrl != None:
            url = response.urljoin(nextUrl)
            req = Request(url=url, callback=self.getCateData, dont_filter=True)
            yield req            

    def getDetailData(self, response):
        print "--- getDetailData ---"

        item = TrustedshopsItem()
        
        item['category'] = ', '.join(response.xpath('//div[@class="hidden-xs hideApp"]//a/text()').extract()).strip()
        if item['category'] == "":
            item['category'] = ', '.join(response.xpath('//div[@class="hidden-xs"]//a/text()').extract()).strip()

        item['company'] = ''.join(response.xpath('//div[@id="provider"]/div[2]/text()').extract()[0]).strip()
        item['street_and_no'] = ''.join(response.xpath('//div[@id="provider"]/div[2]/text()').extract()[1]).strip()
        try:
            zipCodeCity = ''.join(response.xpath('//div[@id="provider"]/div[2]/text()').extract()[2]).strip()
        except:
            return

        try:
            item['zip_code'] = zipCodeCity.split(" ")[0]
        except:
            item['zip_code'] = ""
        try:
            if len(zipCodeCity.split(" ")) == 2:
                item['city'] = zipCodeCity.split(" ")[1]
            elif len(zipCodeCity.split(" ")) == 3:
                item['city'] = zipCodeCity.split(" ")[1] + " " + zipCodeCity.split(" ")[2]
            else:
                item['city'] = zipCodeCity.split(" ")[1] + " " + zipCodeCity.split(" ")[2] + " " + zipCodeCity.split(" ")[3]
        except:
            item['city'] = ""
        item['country'] = ''.join(response.xpath('//div[@id="provider"]/div[2]/text()').extract()[3]).strip()

        try:
            item['register'] = ''.join(response.xpath('//div[@id="provider"]/div[contains(text(), "Handelsregister")]/following-sibling::div[@class="postAddress"]/text() | //div[@id="provider"]/div[contains(text(), "au registre de commerce")]/following-sibling::div[@class="postAddress"]/text()').extract_first()).strip()
        except:
            item['register'] = ""
        try:
            item['manager'] = ''.join(response.xpath('//div[@id="provider"]/div[contains(text(), "Vertretungsberechtigter")]/following-sibling::text() | //div[@id="provider"]/div[contains(text(), "do reprezentowania firmy")]/following-sibling::text() | //div[@id="provider"]/div[contains(text(), "sentant l")]/following-sibling::text()').extract_first()).strip()
        except:
            item['manager'] = ""

        item['contact_street_and_no'] = ''.join(response.xpath('//div[@id="contactData"]/div[2]/text()').extract()[0]).strip()
        contact_zipCodeCity = ''.join(response.xpath('//div[@id="contactData"]/div[2]/text()').extract()[1]).strip()
        try:
            item['contact_zip_code'] = contact_zipCodeCity.split(" ")[0]
        except:
            item['contact_zip_code'] = ""

        try:
            if len(contact_zipCodeCity.split(" ")) == 2:
                item['contact_city'] = contact_zipCodeCity.split(" ")[1]
            elif len(contact_zipCodeCity.split(" ")) == 3:
                item['contact_city'] = contact_zipCodeCity.split(" ")[1] + " " + contact_zipCodeCity.split(" ")[2]
            else:
                item['contact_city'] = contact_zipCodeCity.split(" ")[1] + " " + contact_zipCodeCity.split(" ")[2] + " " + contact_zipCodeCity.split(" ")[3]
        except:
            item['contact_city'] = ""

        contact_city = ''.join(response.xpath('//div[@id="contactData"]/div[2]/text()').extract()[1]).strip()
        item['contact_country'] = ''.join(response.xpath('//div[@id="contactData"]/div[2]/text()').extract()[2]).strip()
        try:
            item['phone'] = ''.join(response.xpath('//div[@id="contactData"]/div[3]/a/text()').extract()).strip()
        except:
            item['phone'] = ""
        try:
            faxTxt = ''.join(response.xpath('//div[@id="contactData"]/div[3]//text()').extract()).strip()
        except:
            faxTxt = ''

        if "Fax" in faxTxt:
            try:
                item['fax'] = ''.join(faxTxt.split("Fax:")[-1]).strip()
            except:
                print "err"
                item['fax'] = ""
        else:
            item['fax'] = ""

        item['email'] = response.xpath('//div[@id="contactData"]//a[@class="shopContactEmail"]/text()').extract_first()
        item['url'] = response.xpath('//div[@id="contactData"]//a[@class="shopLinker"]/@href').extract_first()
        item['shop_name'] = response.xpath('//h2[@class="shopName"]/text()').extract_first()
        if item["shop_name"] == None:
            item['shop_name'] = response.xpath('//h1[@class="shopName"]/text()').extract_first()

        item['shop_zertificate_from'] = response.xpath('//div[@class="col-xs-12 col-sm-6 col-lg-offset-1 col-lg-6 more"]/div[@class="certRow"][1]/div[@class="inputField"]/text()').extract_first()
        item['shop_zertificate_to'] = response.xpath('//div[@class="col-xs-12 col-sm-6 col-lg-offset-1 col-lg-6 more"]/div[@class="certRow"][2]/div[@class="inputField"]/text()').extract_first()

        shop_rating_shipmentTxt = response.xpath('//ul[@id="singleCategoriesList"]/li[1]//span[@class="hidden-xs hidden-sm"]/text()').extract_first()
        try:
            item['shop_rating_shipment'] = re.search('([0-9,.]+)', shop_rating_shipmentTxt, re.I|re.S|re.M).group(1)
        except:
            item['shop_rating_shipment'] = ""

        shop_rating_goodsTxt = response.xpath('//ul[@id="singleCategoriesList"]/li[2]//span[@class="hidden-xs hidden-sm"]/text()').extract_first()
        try:
            item['shop_rating_goods'] = re.search('([0-9,.]+)', shop_rating_goodsTxt, re.I|re.S|re.M).group(1)
        except:
            item['shop_rating_goods'] = ""

        shop_rating_serviceTxt = response.xpath('//ul[@id="singleCategoriesList"]/li[3]//span[@class="hidden-xs hidden-sm"]/text()').extract_first()
        try:
            item['shop_rating_service'] = re.search('([0-9,.]+)', shop_rating_serviceTxt, re.I|re.S|re.M).group(1)
        except:
            item['shop_rating_service'] = ""
        # try:
        #     shop_rating_total = (float(item['shop_rating_shipment']) + float(item['shop_rating_goods']) + float(item['shop_rating_service']))/3
        #     item['shop_rating_total'] = "%.02f"%shop_rating_total
        # except:
        #     item['shop_rating_total'] = ""

        # shop_rating_total = response.xpath('//ul[@id="ratingSummaryBar"]/li[@data-targetcategory="A"]/ul/li[@class="ratingSummaryBarRowValue hidden-sm"]/text()').extract_first()
        shop_rating_noTxt = response.xpath('//div[@class="row ratingSummary-legalText"]//strong/text()').extract_first()
        try:
            item['shop_rating_total'] = re.search('([0-9.]+)', shop_rating_noTxt.replace(" ", ""), re.I|re.S|re.M).group(1)
        except:
            item['shop_rating_total'] = ""

        item["check_url"] = response.meta["check_url"]
        # print item
        yield item