# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TrustedshopsItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    category = scrapy.Field()
    company = scrapy.Field()
    street_and_no = scrapy.Field()
    zip_code = scrapy.Field()
    city = scrapy.Field()
    country = scrapy.Field()
    register = scrapy.Field()
    manager = scrapy.Field()
    # contact_street_and_no = scrapy.Field()
    # contact_zip_code = scrapy.Field()
    # contact_city = scrapy.Field()
    # contact_country = scrapy.Field()
    phone = scrapy.Field()
    # fax = scrapy.Field()
    email = scrapy.Field()
    url = scrapy.Field()
    # shop_name = scrapy.Field()
    shop_zertificate_from = scrapy.Field()
    shop_zertificate_to = scrapy.Field()
    shop_rating_shipment = scrapy.Field()
    shop_rating_goods = scrapy.Field()
    shop_rating_service = scrapy.Field()
    shop_rating_total = scrapy.Field()
    # shop_rating_no = scrapy.Field()
    check_url = scrapy.Field()